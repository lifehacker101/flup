#!/usr/bin/python3

#
# flup
# Simple file upload server
# Copyright (C) 2024 Han Sol Jin
#
# SPDX-License-Identifier: 0BSD
#

import asyncio
import aiofiles
import os

import functools

from apindex import IndexWriter as apIndexWriter

from typing import Optional, Union, Annotated
from fastapi import FastAPI, HTTPException, Request, File, UploadFile
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles

out_file_path = "browse"

app = FastAPI()
app.mount("/browse", StaticFiles(directory="browse", html = True), name="static")

async def writeApIndex(path):
    loop = asyncio.get_event_loop()
    argv = functools.partial(apIndexWriter.writeIndex, path)
    await loop.run_in_executor(None, argv)

async def create_upload_file(input_file: UploadFile):
    async with aiofiles.open(f"{out_file_path}/{input_file.filename}", 'wb') as out:
        while content := await input_file.read(1024):
            await out.write(content)
    await writeApIndex(out_file_path)
    return

@app.post("/upload")
async def api_create_upload_file(input_file: UploadFile):
    await create_upload_file(input_file)
    return {"status": "ok"}

@app.post("/", include_in_schema=False)
async def html_create_upload_file(input_file: UploadFile):
    await create_upload_file(input_file)
    with open("index.html", 'r') as f:
        return HTMLResponse(f.read().replace("<!-- SUCCESS -->", "<h3>Upload succeeded!</h3>"))

@app.get("/", include_in_schema=False)
async def webpage():
    with open("index.html", 'r') as f:
        return HTMLResponse(f.read())
