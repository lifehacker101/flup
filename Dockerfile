FROM python:3.11-slim-bookworm

ENV HOME /home/flup
RUN useradd -m flup
WORKDIR $HOME
COPY ./requirements.txt .
USER flup

RUN pip3 install -r requirements.txt
RUN mkdir browse
COPY main.py main.py
COPY apindex.py apindex.py
COPY share share
COPY index.html index.html

EXPOSE 8000
CMD ["python3", "-m", "uvicorn", "main:app", "--host", "0.0.0.0"]
